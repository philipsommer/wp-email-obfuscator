<?php
/*
Plugin Name:  Spamsafe email obfuscator
Description:  This plugin creates the [email] tag and double obfuscates the address in a bot unreadable way. 
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/
function obfuscate_email_shortcode( $atts , $content = null ) {
  $substDot = "-...-";
  $substAt = "-.@.-";

  $reversed = strrev($content);
  $substituted = str_replace(".", $substDot, $reversed);
  $substituted = str_replace("@", $substAt, $substituted);
  return '
  <span id="subst-dot" style="display: none;">'.$substDot.'</span>
  <span id="subst-at" style="display: none;">'.$substAt.'</span>
  <a
    id="revobfmail" 
    style="unicode-bidi: bidi-override; direction: rtl;"
    onclick="revobf()">' . antispambot($substituted) . '</a>';
}

function register_script() {
  wp_register_script( 'obfuscate_email', plugins_url('/email_obfuscator.js', __FILE__), array(jquery));
  wp_enqueue_script( 'obfuscate_email');
}

add_action('wp_enqueue_scripts', 'register_script');
add_shortcode( 'email', 'obfuscate_email_shortcode' );
?>
